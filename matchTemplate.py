import cv2
import numpy as np

# 使用する画像の名前取得
image = 'test.png'
img_template = 'test_template.png'

# テンプレートマッチ方法の定義
methods = [cv2.TM_CCOEFF_NORMED, cv2.TM_SQDIFF]

# テンプレートマッチング方法をそれぞれ実行
for method in methods:
    # 画像の読み込み
    img_origin = cv2.imread(image)
    template = cv2.imread(img_template)
    img = np.copy(img_origin)

    # 画像サイズの取得
    height, width, color = img.shape
    height_T, width_T, color_T = template.shape
    
    result = cv2.matchTemplate(img, template, method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    
    top_left = max_loc
    #  TM_SQDIFFは絶対二乗誤差を用いているので最小値の場所を左上とする
    if method == methods[1]:
        top_left = min_loc

    btm_right = (top_left[0] + width_T, top_left[1] + height_T)
    
    # img画像に一致する場所へ線を引く
    cv2.rectangle(img, top_left,btm_right, 255, 2)
    
    # 一致した場所をトリミングする
    imgCut = img_origin[top_left[1]:top_left[1] + height_T,top_left[0]:top_left[0] + width_T, :]
    
    # 画像がピクセル単位であっているか確認
    print("-------------------")
    flag =  np.array_equal(imgCut, template)
    if(flag):
        print("完全一致")
    else:
        print("不一致")
        
    # 差分の計算
    diff = imgCut - template
    
    # 画像の表示
    cv2.imshow("comparison", img)
    cv2.imshow("Extraction", imgCut)
    cv2.imshow("diff", diff)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    print("maxValue:", max_val)
    print("minValue:", min_val)
    print("top_left:", top_left)
    print("テンプレートサイズ:",template.shape)
cv2.imwrite("a.png",imgCut)


import cv2
import numpy as np

# 使用する画像の名前取得
image = 'himawari_template.png'
img_template = 'himawari_template.png'

img_origin = cv2.imread(image)
template = cv2.imread(img_template)
img = img_origin[:template.shape[0],:template.shape[1],:]

flag =  np.array_equal(img, template)
if(flag):
     print("完全一致")
else:
    print("不一致")

cv2.imshow("comparison", img)
cv2.imshow("Extraction", template)

cv2.waitKey(0)
cv2.destroyAllWindows()

print("img", img.shape)
print("template", template.shape)